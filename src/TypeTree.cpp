#include <stdio.h>
#include <tuple>
#include <vector>
#include "tree.hpp"

#define LEFT '4'
#define RIGHT '6'
#define PREVIOUS '8'
#define EXIT '0'

using value_type = uint8_t;

/// Print choice
void print_choice(char choice) {
  switch (choice) {
    case LEFT:
      printf("left ");
      break;

    case RIGHT:
      printf("right ");
      break;

    case PREVIOUS:
      printf("previous ");
      break;
  }
}

/// Print node string
void print_node(char choice, const char* str) {
  print_choice(choice);
  printf("leads to node %s\n", str);
}

/// Print some info when hitting a dead end
void print_dead_end(char choice) {
  print_choice(choice);
  printf("leads to a dead end\n");
}

void vector_left(std::vector<value_type>& v) {
  v.back() = 1;
  v.push_back(0);
  v.push_back(0);
}

void vector_right(std::vector<value_type>& v) {
  v.back() = 1;
  v.push_back(1);
  v.push_back(0);
}

bool vector_previos(std::vector<value_type>& v) {
  if (v.size() >= 3) {
    v.pop_back();
    v.pop_back();
    v.back() = 0;
    return true;
  } else
    return false;
}

void go(char choice, std::vector<value_type>& v) {
  switch (choice) {
    case LEFT:
      vector_left(v);
      break;

    case RIGHT:
      vector_right(v);
      break;

    case PREVIOUS:
      if (!vector_previos(v)) {
        print_dead_end(choice);
        return;
      }
      break;
  }

  auto str{visit(
      [](auto v) -> const char* {
        if constexpr (std::is_same_v<decltype(v), Node>)
          return v.str;
        else
          return nullptr;
      },
      tree,
      gsl::make_span(&v[0], v.size()))};

  if (str)
    print_node(choice, *str);
  else {
    print_dead_end(choice);
    vector_previos(v);
  }
}

int main() {
  printf("       +---+\n"
         "       |   |\n"
         "       | a |\n"
         "       |   |\n"
         "  +----+---+----+\n"
         "+-v-+         +-v-+\n"
         "|   |         |   |\n"
         "| b |         | c |\n"
         "|   |         |   |\n"
         "+---+      +--+---+--+\n"
         "           |         |\n"
         "         +-v-+     +-v-+\n"
         "         |   |     |   |\n"
         "         | d |     | e |\n"
         "         |   |     |   |\n"
         "         +---+--+  +---+\n"
         "                |\n"
         "              +-v-+\n"
         "              |   |\n"
         "              | f |\n"
         "              |   |\n"
         "              +---+\n");

  auto v{std::vector<value_type>{}};
  v.reserve(8);
  v.push_back(0);
  printf("you start at node a\n");
  printf("\n");

  for (;;) {
    char c{getchar()};

    switch (c) {
      case LEFT:
        go(LEFT, v);
        break;

      case RIGHT:
        go(RIGHT, v);
        break;

      case PREVIOUS:
        go(PREVIOUS, v);
        break;

      case EXIT:
        printf("bye\n");
        return 0;
        break;

      default:
        printf("wrong input\n");
        break;
    }

    // get rid of enter
    c = getchar();

    printf("\n");
  }

  return 0;
}
